package com.ivandimitrov.imagedownloader.screens.imagelist.data.remote

import com.ivandimitrov.imagedownloader.api.ApiClient
import com.ivandimitrov.imagedownloader.api.ApiInterface

class ImageListRemoteDataSource(val api: ApiClient) : BaseDataSource() {

    suspend fun getImageListData() = getResult {
        api.apiClient().getImageListData("Client-ID " + ApiInterface.ApiKeys.CLIENT_ID)
    }

}
