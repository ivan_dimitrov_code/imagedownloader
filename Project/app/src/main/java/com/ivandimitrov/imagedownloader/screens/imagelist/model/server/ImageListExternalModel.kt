package com.ivandimitrov.imagedownloader.screens.imagelist.model.server

data class ImageListExternalModel(
    val title: String,
    val images: List<ImageExternalModel>
)