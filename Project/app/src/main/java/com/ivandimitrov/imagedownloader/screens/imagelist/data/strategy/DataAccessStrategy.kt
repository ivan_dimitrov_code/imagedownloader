package com.ivandimitrov.imagedownloader.screens.imagelist.data.strategy

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.ivandimitrov.imagedownloader.screens.imagelist.ui.Resource
import kotlinx.coroutines.Dispatchers

fun <T, A> performGetOperation(
    networkCall: suspend () -> Resource<A>,
    saveCallResult: suspend (A) -> Unit
): LiveData<Resource<T>> =
    liveData(Dispatchers.IO) {
        emit(Resource.loading())
        val responseStatus = networkCall.invoke()

        if (responseStatus.status == Resource.Status.SUCCESS) {
            saveCallResult(responseStatus.data!!)
        } else if (responseStatus.status == Resource.Status.ERROR) {
            emit(Resource.error(responseStatus.message!!))
        }
    }