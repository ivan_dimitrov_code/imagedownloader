package com.ivandimitrov.imagedownloader.screens.imagelist.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.ivandimitrov.imagedownloader.screens.imagelist.data.remote.ImageListModelAdapter
import com.ivandimitrov.imagedownloader.screens.imagelist.data.remote.ImageListRemoteDataSource
import com.ivandimitrov.imagedownloader.screens.imagelist.data.strategy.performGetOperation
import com.ivandimitrov.imagedownloader.screens.imagelist.model.internal.ImageInfo
import com.ivandimitrov.imagedownloader.screens.imagelist.ui.Resource
import kotlinx.coroutines.Dispatchers

class ImageListRepository(private val remoteDataSource: ImageListRemoteDataSource) {
    private var cachedData = mutableListOf<ImageInfo>()

    fun getImageList(): LiveData<Resource<MutableList<ImageInfo>>> = performGetOperation(
        networkCall = { remoteDataSource.getImageListData() },
        saveCallResult = {
            cachedData = ImageListModelAdapter.transformExternalToInternal(it).toMutableList()

            liveData(Dispatchers.IO) {
                emit(Resource.success(cachedData))
            }
        }
    )

}
