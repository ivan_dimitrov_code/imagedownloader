package com.ivandimitrov.imagedownloader.api

import com.ivandimitrov.imagedownloader.screens.imagelist.model.server.ImageExternalServerResponse
import com.ivandimitrov.imagedownloader.screens.imagelist.model.server.ImageListExternalModel
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header

interface ApiInterface {

    @GET("/3/gallery/hot/viral/0.json")
    suspend fun getImageListData(
        @Header("Authorization") token: String,
    ): Response<ImageExternalServerResponse>

    object ApiKeys {
        const val CLIENT_ID = "5af5ac0aeca739c"
    }
}