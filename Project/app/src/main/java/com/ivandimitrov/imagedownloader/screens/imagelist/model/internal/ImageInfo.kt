package com.ivandimitrov.imagedownloader.screens.imagelist.model.internal

data class ImageInfo(
    val imageUrl: String,
    val description: String
)