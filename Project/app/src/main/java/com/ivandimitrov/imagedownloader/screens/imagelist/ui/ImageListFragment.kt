package com.ivandimitrov.imagedownloader.screens.imagelist.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.ivandimitrov.imagedownloader.R
import com.ivandimitrov.imagedownloader.api.ApiClient
import com.ivandimitrov.imagedownloader.screens.imagelist.ImageListViewModel
import com.ivandimitrov.imagedownloader.screens.imagelist.ImageListViewModelFactory
import com.ivandimitrov.imagedownloader.screens.imagelist.data.ImageListRepository
import com.ivandimitrov.imagedownloader.screens.imagelist.data.remote.ImageListRemoteDataSource
import kotlinx.android.synthetic.main.main_fragment.*


class ImageListFragment : Fragment() {

    companion object {
        fun newInstance() = ImageListFragment()
    }

    private lateinit var adapter: ImageListRecyclerViewAdapter

    private lateinit var viewModel: ImageListViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(
            this,
            ImageListViewModelFactory(ImageListRepository(ImageListRemoteDataSource(ApiClient)))
        ).get(
            ImageListViewModel::class.java
        )

        setupRecyclerView()
        setupObservers()
    }

    private fun setupRecyclerView() {
        adapter = ImageListRecyclerViewAdapter(emptyList())
        list.layoutManager = LinearLayoutManager(requireContext())
        list.adapter = adapter
    }

    private fun setupObservers() {
        viewModel.imageList.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    if (!it.data.isNullOrEmpty()) adapter.setItems(ArrayList(it.data))
                }
                Resource.Status.ERROR ->
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()

                Resource.Status.LOADING -> {

                }
            }
        })
    }
}