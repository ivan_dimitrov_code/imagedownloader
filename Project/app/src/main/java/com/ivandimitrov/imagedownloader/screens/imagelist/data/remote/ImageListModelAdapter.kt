package com.ivandimitrov.imagedownloader.screens.imagelist.data.remote

import com.ivandimitrov.imagedownloader.screens.imagelist.model.internal.ImageInfo
import com.ivandimitrov.imagedownloader.screens.imagelist.model.server.ImageExternalServerResponse

object ImageListModelAdapter {

    fun transformExternalToInternal(externalData: ImageExternalServerResponse): MutableList<ImageInfo> {
        val transformedData = mutableListOf<ImageInfo>()
        externalData.data.forEach {
            if (!it.images.isNullOrEmpty()) {
                val url = "https://i.imgur.com/" + it.images[0].id
                ImageInfo(url, it.title)
            }
        }
        return transformedData
    }
}