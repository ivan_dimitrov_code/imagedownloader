package com.ivandimitrov.imagedownloader.screens.imagelist

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.ivandimitrov.imagedownloader.screens.imagelist.data.ImageListRepository

class ImageListViewModelFactory(
    private val imagelistRepository: ImageListRepository
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ImageListViewModel(imagelistRepository) as T
    }
}
