package com.ivandimitrov.imagedownloader.screens.imagelist.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.ivandimitrov.imagedownloader.R
import com.ivandimitrov.imagedownloader.screens.imagelist.model.internal.ImageInfo
import com.squareup.picasso.Picasso

class ImageListRecyclerViewAdapter(
    private var values: List<ImageInfo>
) : RecyclerView.Adapter<ImageListRecyclerViewAdapter.ViewHolder>() {

    fun setItems(values: List<ImageInfo>) {
        this.values = values
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.image_cell, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.title.text = item.description
        Picasso.get().load(item.imageUrl).into(holder.image)
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val title: TextView = view.findViewById(R.id.itemTitle)
        val image: ImageView = view.findViewById(R.id.itemImage)
    }
}