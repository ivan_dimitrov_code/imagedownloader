package com.ivandimitrov.imagedownloader.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.ivandimitrov.imagedownloader.R
import com.ivandimitrov.imagedownloader.screens.imagelist.ui.ImageListFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, ImageListFragment.newInstance())
                .commitNow()
        }
    }
}