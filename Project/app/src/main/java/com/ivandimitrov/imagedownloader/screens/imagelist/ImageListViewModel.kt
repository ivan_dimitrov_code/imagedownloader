package com.ivandimitrov.imagedownloader.screens.imagelist

import androidx.lifecycle.ViewModel
import com.ivandimitrov.imagedownloader.screens.imagelist.data.ImageListRepository

class ImageListViewModel(
    imageListRepository: ImageListRepository,
) : ViewModel() {
    val imageList = imageListRepository.getImageList()
}

