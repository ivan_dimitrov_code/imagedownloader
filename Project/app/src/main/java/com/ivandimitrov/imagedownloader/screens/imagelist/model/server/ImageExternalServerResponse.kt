package com.ivandimitrov.imagedownloader.screens.imagelist.model.server

data class ImageExternalServerResponse(val data: List<ImageListExternalModel>)